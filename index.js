// [SECTION] Creating a simple server using ExpressJS Framework

// Use the require() directive to load the express module/package
// It allows us to access the methods and functions that will help us create a server
const express = require("express");

// Create an application using express
// This creates an express application and stores this in a constant called "app"
// In simple terms, app is now our server
const app = express(); 

const port = 3000;

// Setup for allowing the server to handle data from requests
// In simple terms, it allows your app to read json data
// Methods used are called middlewares
// Middleware is software that provides common services and capabilities to applications outside of what's offered by the operating system
// API management is one of the common application of middlewares
app.use(express.json()); // adds middleware to the app that parses incoming JSON data. This middleware is provided by the "express.json()" function, which allows our application to receive and parse JSON data from requests sent to it.

// allows your application to read data from forms
// By default, information received from the url can only be received as a string or an array
// By applying the option of "extended: true", this allows us to receive information on other data types such as an object which we will use throughout our application.
app.use(express.urlencoded({extended: true})); //adds middleware to the app that parses incoming URL-encoded data.

// [SECTION] Creating Routes 

app.get("/", (request, response) => {

	response.send("Hello World")
});

// POST METHOD
app.post("/", (request,response) => {

	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}`);
});

// Mock Database
let users = [];

// Signup Method
app.post("/signup", (request, response) => {

	if (request.body.username !== "" && request.body.password !== "") {

		users.push(request.body);
		response.send(`User ${request.body.username} is successfully registered!`);
	}

	else {

		response.send(`Please input BOTH username and password!`);
	}

	console.log(request.body);
	
});

// Change Password
app.put("/change-password", (request, response) => {

	// Create a variable that will store the message to be sent back to the client
	let message;

	for(let user = 0; user < users.length; user++) {

		if (request.body.username == users[user].username) {

			users[user].password = request.body.password;
			message = `User ${request.body.username} has successfully change their password`;
			break;
		}

		else {

			message = `User ${request.body.username} does not exist`
		}

	}

	response.send(message);
	console.log(users);

});

// S34 Activity
// Create a GET route that will access the /home route that will print out a simple message
app.get("/home", (request, response) => {

	response.send("Welcome to the home page");
});

// Create a GET route that will access the /users route that will retrieve all the users in the mock database
app.get("/users", (request, response) => {

	response.send(users);
	
});

// STRETCH GOAL
// Create a DELETE route that will access the /delete-item route to remove a user from the mock database.
app.delete ("/delete-user", (request, response) => {

	let messageDelete;

	if (users.length > 0) {
		for (user = 0; user < users.length; user++) {
			if (users[user].username == request.body.username) {
				users.splice (user, 1);
				messageDelete = `User ${request.body.username} has been deleted.`;
				break;

			} else {
				messageDelete = `User does not exist.`;
			}
		};

	} else {
		messageDelete = `No users found.`;
	}

	response.send(messageDelete);
	
});


app.listen(port, () => console.log(`Server is listening at localhost: ${port}`));